## grails configuration for gelf:


level:  6 <br>
host: "hostname" <br>
version: "1.1" <br>
@version: "1" <br>
@timestamp: "actual date" <br>
source_host: "source ip" <br>
application: "appname" <br>
type: "gelf" <br>

## Grails 3
```bash
/data/logs/rrzepp$ /opt/logstash/bin/logstash -e 'input { gelf {port => "PORT" type => gelf} } output { stdout {codec => rubydebug { metadata => true }} }'
ssh -N -L 6667:localhost:6667 idm-mom
nc -l -p 6667  /tmp/fifo
nc -l -u -p 5141  /tmp/fifo

sourceFileName:  grails 3.x "StdSchedulerFactory.java"
sourceMethodName: grails 3.x "instantiate"
sourceClassName: grails 3.x "org.quartz.impl.StdSchedulerFactory"
sourceLineNumber: grails 3.x 1331
levelName:  grails 3.x "INFO"
loggerName:  grails 3.x "org.quartz.impl.StdSchedulerFactory"
threadName:  grails 3.x "main"

/build.gradle
compile "de.appelgriepsch.logback:logback-gelf-appender:1.2"


grails-app/conf/logback.groovy
import de.appelgriepsch.logback.GelfAppender

def appName = Metadata.current.getApplicationName()

appender('LOGHOST', GelfAppender) {
    filter = new ThresholdFilter([level: Level.INFO])
    //server = 'log-server'
    server = 'localhost'       
    //hostName = java.net.InetAddress.getLocalHost().getHostName()
    port = 5141                
    includeLevelName = true    
    additionalFields = "application=${appName}"
}

root(INFO, ['STDOUT', 'FILE', 'SIFT', 'LOGHOST'])

grails 3.x
Example Messge:
{
             "version" => "1.1",
                "host" => "hostname",
               "level" => 6,
            "@version" => "1",
          "@timestamp" => "2016-10-14T08:04:08.811Z",
         "source_host" => "source_host_ip",
             "message" => "Quartz scheduler version: 2.2.2",
      "sourceFileName" => "StdSchedulerFactory.java",
    "sourceMethodName" => "instantiate",
     "sourceClassName" => "org.quartz.impl.StdSchedulerFactory",
    "sourceLineNumber" => 1331,
           "levelName" => "INFO",
        "{application" => "dip}",
          "loggerName" => "org.quartz.impl.StdSchedulerFactory",
          "threadName" => "main",
                "type" => "gelf"
}
```

## grails 2

```bash
facility:  grails 2.x  "gelf-java"
timestamp:  grails 2.x "1476437100.237"
timestampMs:  grails 2.x "1476437100237"
logger:   grails 2.x "org.apache.catalina.core.ContainerBase.[Tomcat].[localhost].[/foo255]"
thread:   grails 2.x "http-bio-8080-exec-9"

grails-app/conf/BuildConfig.groovy
dependencies {
 runtime("org.graylog2:gelfj:1.1.14")

grails-app/conf/Config.groovy
}
appenders {
 appender new org.graylog2.log.GelfAppender(
  name:'loghost',
  threshold: org.apache.log4j.Level.INFO,
  graylogHost: 'log-host',
  graylogPort: PORT,
  extractStacktrace: true,
  addExtendedInformation: true,
  includeLocation: false,
  additionalFields:"{'application': 'eprov'}"
 )
}
root { info 'file', 'log4mongoAppender', 'loghost' }

grails 2.x 
Example Message:
{
             "version" => "1.1",
                "host" => "host",
               "level" => 6,
            "@version" => "1",
          "@timestamp" => "2016-10-14T08:04:08.811Z",
         "source_host" => "source_host_ip",
             "message" => "Quartz scheduler version: 2.2.2",
      "sourceFileName" => "StdSchedulerFactory.java",
    "sourceMethodName" => "instantiate",
     "sourceClassName" => "org.quartz.impl.StdSchedulerFactory",
    "sourceLineNumber" => 1331,
           "levelName" => "INFO",
        "{application" => "appname}",
          "loggerName" => "org.quartz.impl.StdSchedulerFactory",
          "threadName" => "main",
                "type" => "gelf"
}

```
