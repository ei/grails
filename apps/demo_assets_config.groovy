assets {
    minifyJs = true
    minifyCss = true
    //enableSourceMaps = false //(default is true)

    minifyOptions = [
            languageMode: 'ES6', // languageIn
            targetLanguage: 'ES5', //Can go from ES6 to ES5 for those bleeding edgers // languageOut
    ]

    excludes = [
            // general rules
            "**/*.md",
            "**/*.txt",

            // select2: js,css (Plugin: belay)
            "**/select2-*/docs/**",
            "**/select2-*/src/**",
            "**/select2-*/tests/**",
            "**/select2-*/dist/js/i18n/*.js",

            // codemirror: js,css (Plugin: encore => grailsCmeditor)
            "**/codemirror/demo/**",
            "**/codemirror/doc/**",
            "**/codemirror/mode/**",
            "**/codemirror/test/**",
            "**/codemirror/theme/**",

            // bootstrap-vue-dev: js,css (Plugin: slay)
            "**/bootstrap-vue-dev-*/scripts/build.scss",
            "**/bootstrap-vue-dev-*/src/**",

            // bootstrap: js,css (Plugin: slay)
            "**/bootstrap-*/.circleci/**",
            "**/bootstrap-*/.github/**",
            "**/bootstrap-*/_data/**",
            "**/bootstrap-*/_includes/**",
            "**/bootstrap-*/_layouts/**",
            "**/bootstrap-*/_plugins/**",
            "**/bootstrap-*/build/**",
            "**/bootstrap-*/docs/**",
            "**/bootstrap-*/js/src/**",
            "**/bootstrap-*/js/tests/**",
            "**/bootstrap-*/nuget/**",
            "**/bootstrap-*/nuxt/**",
            "**/bootstrap-*/test/**",
            "**/bootstrap-*/test/templates/**",
            "**/bootstrap-*/tests/**",
            "**/bootstrap-*/site/**",

            "**/bootstrap-*/.babelrc.js",
            "**/bootstrap-*/.browserslistrc",
            "**/bootstrap-*/.editorconfig",
            "**/bootstrap-*/.eslintignore",
            "**/bootstrap-*/.gitattributes",
            "**/bootstrap-*/.gitignore",
            "**/bootstrap-*/.stylelintignore",
            "**/bootstrap-*/.stylelintrc",
            "**/bootstrap-*/CNAME",
            "**/bootstrap-*/favicon.ico",
            "**/bootstrap-*/Gemfile",
            "**/bootstrap-*/Gemfile.lock",
            "**/bootstrap-*/gulpfile.js",
            "**/bootstrap-*/index.html",
            "**/bootstrap-*/LICENSE",
            "**/bootstrap-*/package.js",
            "**/bootstrap-*/robots.txt",
            "**/bootstrap-*/sw.js",
            "**/bootstrap-*/*.json",
            "**/bootstrap-*/*.yml",
            "**/bootstrap-*/*.md",

            // fontawesome-free: js,css (Plugin: slay)
            "**/fontawesome-free-*/less/**",
            "**/fontawesome-free-*/metadata/**",
            "**/fontawesome-free-*/LICENSE.txt",
            "**/fontawesome-free-*/web-fonts-with-css/css/**",
            "**/fontawesome-free-*/web-fonts-with-css/less/**",

            // general slay: js,css (Plugin: slay)
            //"*.html",
            "**/*.gemspec",
            "**/.gitignore",
            "**/LICENSE",
            "**/README",
            "**/Rakefile",
            "**/Gemfile",
            //"**/.*",
            // ~slay


            /* others - not used in project */
            // jQuery-File-Upload: js, css (Plugin: belay)
            "**/jQuery-File-Upload**",
    ]

    includes = [
            // select2 (Plugin: belay)
            "**/select2-*/dist/js/i18n/en.js",
            "**/select2-*/dist/js/i18n/de.js",

            // codemirror modes - js (Plugin: encore => grailsCmeditor)
            "**/codemirror/mode/meta.js",
            "**/codemirror/mode/htmlmixed/*.js",
            "**/codemirror/mode/htmlembedded/*.js",
            "**/codemirror/mode/css/*.js",
            "**/codemirror/mode/xml/*.js",
            "**/codemirror/mode/javascript/*.js",
            "**/codemirror/mode/groovy/*.js",
            "**/codemirror/mode/clike/*.js",
            "**/codemirror/mode/properties/*.js",

            // codemirror themes - css (Plugin: encore => grailsCmeditor)
            "**/codemirror/theme/eclipse.css",
            "**/codemirror/theme/idea.css",
            "**/codemirror/theme/lesser-dark.css",
            "**/codemirror/theme/monokai.css",
            "**/codemirror/theme/night.css",
            "**/codemirror/theme/the-matrix.css",
            "**/codemirror/theme/twilight.css",
    ]

}