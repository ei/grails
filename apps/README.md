# Application Templates
This folder contains information related to Grails application development.

## **WARNING!**
**Application template files were moved to a separate project for better versioning.**
**You can find the all artifacts under [grails/app-template](https://gitlab.rrze.fau.de/grails/app-template).**