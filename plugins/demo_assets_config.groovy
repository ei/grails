
def excludePatterns = [
        "**/jQuery-File-Upload-*/server/**",
        "**/jQuery-File-Upload-*/cors/**",
        "**/jQuery-File-Upload-*/test/**",

        "**/select2-*/docs/**",
        "**/select2-*/src/**",
        "**/select2-*/tests/**",
        "**/select2-*/vendor/**",

        "**/bootstrap-sass-*/lib/**",
        "**/bootstrap-sass-*/tasks/**",
        "**/bootstrap-sass-*/templates/**",
        "**/bootstrap-sass-*/test/**",

        "**/select2*/docs/**",
        "**/select2*/tests/**",
        "**/select2*/src/**",

        "**/fontawesome-free-*/advanced-options/**",
        "**/fontawesome-free-*/svg-with-js/**",
        "**/fontawesome-free-*/use-one-desktop/**",
        "**/fontawesome-free-*/web-fonts-with-css/css/**",
        "**/fontawesome-free-*/web-fonts-with-css/less/**",

        "**/*.md",
        "*.html",
        "**/*.txt",
        "**/*.gemspec",
        "**/.gitignore",
        "**/LICENSE",
        "**/README",
        "**/Rakefile",
        "**/Gemfile",
        "**/.*",
]

assets {
    packagePlugin = true

    minifyJs = false
    minifyCss = false

    maxThreads = 1
    excludes = excludePatterns
}

jar {
    exclude (excludePatterns)
}
