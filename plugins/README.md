# Plugin Templates
This folder contains information related to Grails plugins development.

## **WARNING!**
**Plugin template files were moved to a separate project for better versioning.**
**You can find the all artifacts under [grails/plugin-template](https://gitlab.rrze.fau.de/grails/plugin-template).**