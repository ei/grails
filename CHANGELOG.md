# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

The **Unreleased** section is used for keeping track of any changes,
so people can see what changes they might expect in upcoming releases.

Basic rules:
 - One sub-section per version.
 - List releases in reverse-chronological order (newest on top).
 - Write all dates in YYYY-MM-DD format. (Example: 2019-10-24)

Keywords which can be used for releases:
 - **Added** - for new features.
 - **Changed** - for changes in existing functionality.
 - **Deprecated** - for once-stable features removed in upcoming releases.
 - **Removed** - for deprecated features removed in this release.
 - **Fixed** - for any bug fixes.
 - **Security** - to invite users to upgrade in case of vulnerabilities.

## [Unreleased] - currently

## [0.2.0] - 2020-01-21 13:59
### Removed
 - **BREAKING**: app templates moved to a separate project - grails/app-template

## [0.1.0] - 2020-01-20 16:47
### Removed
 - **BREAKING**: plugin templates moved to a separate project - grails/plugin-template
### Added
 - GitLab CI conf link for apps
 - wget instructions in apps README.md
 
## [0.0.5] - 2020-01-17 14:32
### Added
 - .gitignore
### Changed
 - plugins/demo_assets_config.groovy - removed version for fontawesome excludes
 - plugins and app: .gitlab-ci.yml - artifacts: expire_in: 1h

## [0.0.4] - 2019-12-17
### Added
 - README.md(project): supported grails versions
### Fixed
 - CHANGELOG.md(plugins, apps) - removed svalbard reference

## [0.0.3] - 2019-12-17
### Added 
 - TEMPLATE_README.md(apps/plugins): 'General Info' section
### Changed
 - TEMPLATE_README.md(apps/plugins): 'Dependencies notes' section
 - README.md(project): Text description for sections
 - **BREAKING**: renamed **config** folder to **install**
### Fixed
 - .gitlab-ci.yml(plugins & apps): build pages for master and not for dev branch

## [0.0.2] - 2019-12-16
### Fixed
 - CHANGELOG links from grails/svalbard to ei/grails

## [0.0.1] - 2019-12-16
### Added
 - Everything (first unofficial release)
 
[Unreleased]: https://gitlab.rrze.fau.de/ei/grails/compare/v0.2.0...dev
[0.2.0]: https://gitlab.rrze.fau.de/ei/grails/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.rrze.fau.de/ei/grails/compare/v0.0.5...v0.1.0
[0.0.5]: https://gitlab.rrze.fau.de/ei/grails/compare/v0.0.4...v0.0.5
[0.0.4]: https://gitlab.rrze.fau.de/ei/grails/compare/v0.0.3...v0.0.4
[0.0.3]: https://gitlab.rrze.fau.de/ei/grails/compare/v0.0.2...v0.0.3
[0.0.2]: https://gitlab.rrze.fau.de/ei/grails/compare/v0.0.2...v0.0.1
[0.0.1]: https://gitlab.rrze.fau.de/ei/grails/-/tags/v0.0.1