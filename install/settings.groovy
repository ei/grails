grails.project.repos.default = "rrzeArtifactory"
 
// disable nagging about not having committed all changes...
grails.release.scm.enabled = false

rrzeRepo.url = 'https://www.idm.fau.de/artifactory'
rrzeRepo.username = 'MYUSERNAME'
rrzeRepo.password = 'MYKEY'

grails.project.repos.rrzeArtifactory.id = 'rrzeArtifactory'
grails.project.repos.rrzeArtifactory.url = "${rrzeRepo.url}/repo"
grails.project.repos.rrzeArtifactory.username = rrzeRepo.username
grails.project.repos.rrzeArtifactory.password = rrzeRepo.password

grails.project.repos.rrzePluginReleases.id = 'rrzePluginReleases'
grails.project.repos.rrzePluginReleases.url = "${rrzeRepo.url}/plugins-release-local/"
grails.project.repos.rrzePluginReleases.username = rrzeRepo.username
grails.project.repos.rrzePluginReleases.password = rrzeRepo.password

grails {
  profiles {
    repositories {
      rrzeArtifactory {
        id = 'rrzeArtifactory'
        url = "${rrzeRepo.url}/repo"
        username = rrzeRepo.username
        password = rrzeRepo.password
      }
    }
  }
}

println "#"*80
println " ... Maven config for user ${grails.project.repos.rrzeArtifactory.username}@${grails.project.repos.rrzeArtifactory.id} was successfully loaded ..."
println "     ... You can check the repository here: ${grails.project.repos.rrzeArtifactory.url}"
println "     ... Publish repository is registered as: ${grails.project.repos.rrzePluginReleases.url}"
println "     ... Please do not use a clear text password in this file."
println "         ... In Artifactory click on your username unlock the UI and generate an 'API Key' or an 'Encrypted Password'."
println "         ... or login and follow this link: ${rrzeRepo.url}/webapp/profile.html"
println "-"*80
