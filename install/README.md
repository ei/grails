# Grails Configuration
This folder contains information related to configuring Grails.

## Maintainers
- Primary: Krasimir Zhelev - @unrz205, krasimir.zhelev@fau.de
- Delegates: 
  - Frank Tröger - @unrz157, frank.troeger@fau.de
  - Niroj Dongol - @sinidong, niroj.dongol@fau.de

## License
All the content in this repository is licensed under the [Apache 2](LICENSE.txt) license.

## Contributing
If you wish to contribute to this project please read first the [CONTRIBUTING](CONTRIBUTING.md) file.

## Installation
Please use [sdkman](https://sdkman.io/) to manage your Grails installation. 
You can find instructions how to do that on the [Grails Download Page](https://grails.org/download.html).

## Configuration
In order to gain access to our in house [artifactory](https://www.idm.fau.de/artifactory) server you have to configure your Grails properly.

