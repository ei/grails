# Grails Documentation Repository
This repository contains basic configuration files ant templates related to developing Grails applications and plugins.

## Maintainers
- Primary: Krasimir Zhelev - @unrz205, krasimir.zhelev@fau.de
- Delegates: 
  - Frank Tröger - @unrz157, frank.troeger@fau.de

## General Info
Current state of internal Grails support:
 - Supported: 
   - Grails 3.3.x
 - In adoption:
   - Grails 4.0.x
 - Deprecated:
   - Grails < 3.3.x
   
Every developer is free to chose a Grails as long as it does not have security issues.
Plugins used for more than one developers must support the version marked as **Supported**.

## License
All the content in this repository is licensed under the [Apache 2](LICENSE.txt) license.

## Contributing
If you wish to contribute to this project please read first the [CONTRIBUTING](CONTRIBUTING.md) file.

## Build
This repository cannot be build - it serves documentation purposes mainly.

## Releases
For a list of all available releases check the project [tags](https://gitlab.rrze.fau.de/ei/grails/-/tags).
Please refer to the [CHANGELOG](CHANGELOG.md) if you need a chronologically ordered list of notable changes for each version.

## Content

### Grails Installation
A detailed description how to install Grails can be found in the folder [install](install).
### Application Info
A detailed documentation related to application development as well as several useful templates can be found in the folder [apps](apps).
### Plugin Info
A detailed documentation related to plugin development as well as several useful templates can be found in the folder [plugins](plugins)
### Gelf Logstash for Grails
The [gelf-conf](gelf-conf) folder contains configuration examples for connecting logastash with Grails by using gelf.
